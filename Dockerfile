FROM mla132/jre-17-slim

EXPOSE 8084

COPY target/university-app.jar /opt/app.jar

CMD java -jar /opt/app.jar --spring.profiles.active=cloud
