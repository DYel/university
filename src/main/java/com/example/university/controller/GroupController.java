package com.example.university.controller;


import com.example.university.dto.CreateFacultyDto;
import com.example.university.dto.CreateGroupDto;
import com.example.university.dto.FacultyDto;
import com.example.university.dto.GroupDto;
import com.example.university.facade.GroupFacade;
import com.example.university.service.FacultyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(value="/api/v1/universities/", produces = MediaType.APPLICATION_JSON_VALUE)
public class GroupController {
    private final GroupFacade groupFacade;
    @PostMapping("/{universityId}/faculties/{facultyId}/groups")
    public ResponseEntity<GroupDto> create (@PathVariable Integer universityId, @PathVariable Integer facultyId,
                                            @RequestBody CreateGroupDto createGroupDto) {
        if (createGroupDto == null ) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        GroupDto groupDto = groupFacade.create( universityId, facultyId, createGroupDto);
        return new ResponseEntity<>(groupDto, HttpStatus.CREATED);
    }

    @PutMapping("/faculties/groups")
    public ResponseEntity<GroupDto> update (@RequestBody GroupDto groupDto) {
        if (groupDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        GroupDto updatedGroupDto = groupFacade.update(groupDto);
        return new ResponseEntity<>(updatedGroupDto, HttpStatus.OK);
    }
    @GetMapping("/{universityId}/faculties/{facultyId}/{id}")
    public ResponseEntity<GroupDto> getFacultyById(@PathVariable Integer universityId,
                                                   @PathVariable Integer facultyId, @PathVariable UUID id) {
        GroupDto groupDto = groupFacade.findById(universityId, facultyId, id);
        return new ResponseEntity<>(groupDto, HttpStatus.OK);
    }

    @DeleteMapping("/{universityId}/faculties/{facultyId}/{id}")
    public ResponseEntity<GroupDto> deleteFacultyById(@PathVariable Integer universityId,
                                                      @PathVariable Integer facultyId, @PathVariable UUID id) {
        GroupDto groupDto = groupFacade.deleteById(id);
        return new ResponseEntity<>(groupDto, HttpStatus.OK);
    }

}

