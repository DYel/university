package com.example.university.controller;

import com.example.university.dto.CreateUniversityDto;
import com.example.university.dto.UniversityDto;
import com.example.university.facade.UniversityFacade;
import com.example.university.model.University;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value="/api/v1/universities", produces = MediaType.APPLICATION_JSON_VALUE)
public class UniversityController {

    private final UniversityFacade universityFacade;

    @GetMapping("/{id}")
    public ResponseEntity<UniversityDto> GetUniversityById(@PathVariable Integer id) {
        UniversityDto universityDto = universityFacade.findById(id);
        return new ResponseEntity<>(universityDto, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<UniversityDto> create (@RequestBody CreateUniversityDto createUniversityDto) {
        if (createUniversityDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        UniversityDto universityDto = universityFacade.create(createUniversityDto);
        return new ResponseEntity<>(universityDto, HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity<UniversityDto> update (@RequestBody UniversityDto universityDto) {
        if (universityDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        UniversityDto requestResult = universityFacade.update(universityDto);
        return new ResponseEntity<>(requestResult, HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<UniversityDto> delete (@RequestParam Integer id) {
        UniversityDto requestResult = universityFacade.deleteById(id);
        return new ResponseEntity<>(requestResult, HttpStatus.OK);
    }

}
