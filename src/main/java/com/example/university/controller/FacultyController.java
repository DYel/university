package com.example.university.controller;


import com.example.university.dto.CreateFacultyDto;
import com.example.university.dto.FacultyDto;
import com.example.university.facade.FacultyFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value="/api/v1/universities", produces = MediaType.APPLICATION_JSON_VALUE)
public class FacultyController {
    private final FacultyFacade facultyFacade;

    @PostMapping("/{universityId}/faculties")
    public ResponseEntity<FacultyDto> create (@PathVariable Integer universityId, @RequestBody CreateFacultyDto createFacultyDto) {
        if (createFacultyDto == null ) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        FacultyDto facultyDto = facultyFacade.create(universityId, createFacultyDto);
        return new ResponseEntity<>(facultyDto, HttpStatus.CREATED);
    }

    @GetMapping("/{universityId}/faculties/{id}")
    public ResponseEntity<FacultyDto> findById(@PathVariable Integer universityId, @PathVariable Integer id) {
        FacultyDto facultyDto = facultyFacade.findById(universityId, id);
        return new ResponseEntity<>(facultyDto, HttpStatus.OK);
    }

    @PutMapping("/faculties")
    public ResponseEntity<FacultyDto> update (@RequestBody FacultyDto facultyDto) {
        if(facultyDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        FacultyDto updatedFacultyDto = facultyFacade.update(facultyDto);
        return new ResponseEntity<>(updatedFacultyDto, HttpStatus.OK);
    }

    @GetMapping("/{universityId}/faculties/most-popular")
    public ResponseEntity<FacultyDto> findMostPopularFaculty(@PathVariable Integer universityId) {
        FacultyDto facultyDto = facultyFacade.findMostPopularFaculty(universityId);
        return new ResponseEntity<>(facultyDto, HttpStatus.OK);
    }


    @DeleteMapping("/{universityId}/faculties/")
    public ResponseEntity<FacultyDto> deleteFacultyById(@PathVariable Integer universityId, @PathVariable Integer facultyId) {
        FacultyDto facultyDto = facultyFacade.deleteById(facultyId);
        return new ResponseEntity<>(facultyDto, HttpStatus.OK);
    }

}

