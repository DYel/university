package com.example.university.converter;

import com.example.university.dto.FacultyDto;
import com.example.university.dto.GroupDto;
import com.example.university.model.Faculty;
import com.example.university.model.Group;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GroupToDtoConverter implements Converter<Group, GroupDto> {
    public GroupDto convert(Group group) {
        return GroupDto.builder()
                .id(group.getId())
                .title(group.getTitle())
                .universityId(group.getUniversityId())
                .facultyId(group.getFacultyId())
                .build();
    }

}
