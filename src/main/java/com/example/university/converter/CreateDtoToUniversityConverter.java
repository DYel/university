package com.example.university.converter;

import com.example.university.dto.CreateUniversityDto;
import com.example.university.model.Faculty;
import com.example.university.model.University;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CreateDtoToUniversityConverter implements Converter<CreateUniversityDto, University> {
    List<Faculty> faculties = new ArrayList<>();
    public University convert (CreateUniversityDto dto) {
        return University.builder()
                .title(dto.getTitle())
                .address(dto.getAddress())
                .email(dto.getEmail())
                .faculties(faculties)
                .build();
    }

}
