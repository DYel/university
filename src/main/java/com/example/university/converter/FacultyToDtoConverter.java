package com.example.university.converter;

import com.example.university.dto.FacultyDto;
import com.example.university.dto.GroupDto;
import com.example.university.model.Faculty;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class FacultyToDtoConverter implements Converter<Faculty, FacultyDto> {
    private final GroupToDtoConverter converter;
    public FacultyDto convert(Faculty faculty) {
        List<GroupDto> groupsDto = new ArrayList<>();
        if (faculty.getGroups() != null && faculty.getGroups().size() > 0) {
            groupsDto = faculty.getGroups().stream().map(group -> converter.convert(group))
                    .collect(Collectors.toList());
        }
        return FacultyDto.builder()
                .id(faculty.getId())
                .title(faculty.getTitle())
                .universityId(faculty.getUniversityId())
                .groupsDto(groupsDto)
                .build();
    }

}
