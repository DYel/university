package com.example.university.converter;

import com.example.university.dto.CreateFacultyDto;
import com.example.university.dto.FacultyDto;
import com.example.university.model.Faculty;
import com.example.university.model.Group;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DtoToFacultyConverter implements Converter<FacultyDto, Faculty> {
    private final DtoToGroupConverter converter;

    public Faculty convert(FacultyDto dto) {
        List<Group> groups = new ArrayList<>();
        groups = dto.getGroupsDto().stream().map(groupDto -> converter.convert(groupDto))
                .collect(Collectors.toList());

        return Faculty.builder()
                .id(dto.getId())
                .title(dto.getTitle())
                .universityId(dto.getUniversityId())
                .groups(groups)
                .build();
    }
}
