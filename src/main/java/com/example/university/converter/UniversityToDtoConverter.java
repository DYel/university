package com.example.university.converter;

import com.example.university.dto.FacultyDto;
import com.example.university.dto.UniversityDto;
import com.example.university.model.Faculty;
import com.example.university.model.University;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UniversityToDtoConverter implements Converter<University, UniversityDto> {
    private final FacultyToDtoConverter converter;
    public UniversityDto convert(University university) {
        List<FacultyDto> facultyDtoList = new ArrayList<>();
        facultyDtoList = university.getFaculties().stream().map(faculty -> converter.convert(faculty))
                .collect(Collectors.toList());

        return UniversityDto.builder()
                .id(university.getId())
                .title(university.getTitle())
                .address(university.getAddress())
                .email(university.getEmail())
                .facultiesDto(facultyDtoList)
                .build();
    }

}
