package com.example.university.converter;

import com.example.university.dto.FacultyDto;
import com.example.university.dto.GroupDto;
import com.example.university.model.Faculty;
import com.example.university.model.Group;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DtoToGroupConverter implements Converter<GroupDto, Group> {
    public Group convert (GroupDto dto) {
        return Group.builder()
                .id(dto.getId())
                .title(dto.getTitle())
                .universityId(dto.getUniversityId())
                .facultyId(dto.getFacultyId())
                .build();
    }
}
