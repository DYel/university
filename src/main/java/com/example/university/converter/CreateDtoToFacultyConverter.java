package com.example.university.converter;

import com.example.university.dto.CreateFacultyDto;
import com.example.university.dto.CreateUniversityDto;
import com.example.university.model.Faculty;
import com.example.university.model.University;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CreateDtoToFacultyConverter implements Converter<CreateFacultyDto, Faculty> {
    public Faculty convert (CreateFacultyDto dto) {
        return Faculty.builder()
                .title(dto.getTitle())
                //.universityId(dto.getUniversityId())
                .build();
    }
}
