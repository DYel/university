package com.example.university.converter;

import com.example.university.dto.UniversityDto;
import com.example.university.model.Faculty;
import com.example.university.model.University;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DtoToUniversityConverter implements Converter<UniversityDto, University> {
    private final DtoToFacultyConverter dtoToFacultyConverter;

    public University convert (UniversityDto dto) {
        List<Faculty> facultyList = new ArrayList<>();
        facultyList = dto.getFacultiesDto().stream().map(facultyDto -> dtoToFacultyConverter.convert(facultyDto))
                .collect(Collectors.toList());
        return University.builder()
                .id(dto.getId())
                .title(dto.getTitle())
                .address(dto.getAddress())
                .email(dto.getEmail())
                .faculties(facultyList)
                .build();
    }
}
