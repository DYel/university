package com.example.university.converter;

import com.example.university.dto.CreateFacultyDto;
import com.example.university.dto.CreateGroupDto;
import com.example.university.model.Faculty;
import com.example.university.model.Group;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CreateDtoToGroupConverter implements Converter<CreateGroupDto, Group> {
    public Group convert (CreateGroupDto dto) {
        return Group.builder()
                .title(dto.getTitle())
                //.facultyId(dto.getFacultyId())
                .build();
    }
}
