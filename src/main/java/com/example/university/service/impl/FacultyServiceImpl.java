package com.example.university.service.impl;

import com.example.university.exceptions.NotFoundException;
import com.example.university.model.Faculty;
import com.example.university.model.Group;
import com.example.university.model.University;
import com.example.university.repository.FacultyRepository;
import com.example.university.repository.UniversityRepository;
import com.example.university.service.FacultyService;;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
@Service
@RequiredArgsConstructor
public class FacultyServiceImpl implements FacultyService {
    private  final FacultyRepository facultyRepository;
    private  final UniversityRepository universityRepository;
    public Faculty create (Faculty faculty) {
//        University university = universityRepository.findById(faculty.getUniversityId())
//                .orElseThrow(() -> new NotFoundException("University with id" +
//                        faculty.getUniversityId() + "not found" ));
//        university.getFaculties().add(faculty);
//        universityRepository.saveAndFlush(university);
//        List<Faculty> faculties = universityRepository.findById(faculty.getUniversityId()).get()
//                .getFaculties();
//
//        return faculties.get(faculties.size()-1);
        return facultyRepository.save(faculty);  //cам добавит faculty в List<Faculty> (in University)
    }

    @Override
    @Transactional  //автоматом сохранит все изменения
    public Faculty update(Faculty faculty) {
        Integer id = faculty.getUniversityId();                   //получили Id универа из входящего факультета
        University university = universityRepository.findById(id) // получили универ по этому Id
                .orElseThrow(() -> new NotFoundException("Can't find university with id " + id));

        Faculty oldFaculty = university.getFaculties().stream()//получаем факультет по faculty.Id для update
                .filter(f -> faculty.getId().equals(f.getId())).findFirst()
                .orElseThrow(() -> new NotFoundException("Can't find faculty with id " + faculty.getId()));
        for (Group group: oldFaculty.getGroups()) { //проверяем кол-во совпадений эл-в входящей коллекции в текущей
            long count = faculty.getGroups().stream()
                    .filter(g -> g.getTitle().equals(group.getTitle()))
                    .count();
            if (count > 1) {  //если во входящей каллекции для update 2 одинаковых титла эл-та, то это ошибка
                throw new NotFoundException("This faculty contains group with title " + group.getTitle());
            }
        }

        oldFaculty.setTitle(faculty.getTitle());                  //сохраняем в oldFaculty все поля из faculty
        oldFaculty.setUniversityId(faculty.getUniversityId());
        oldFaculty.getGroups().clear();
        oldFaculty.setGroups(faculty.getGroups());//Hibernate обновит и сохранит автоматом лист факультетов

        return facultyRepository.save(faculty);
    }

    @Override
    public Faculty findById(Integer universityId, Integer id) {
        Faculty faculty = facultyRepository.findById(id).orElseThrow(() -> new NotFoundException("Can't find faculty with id " + id));
        if (!faculty.getUniversityId().equals(universityId)) {
            throw new NotFoundException("Incorrect universityId:" + universityId);
        }
        return faculty;
    }
    @Override
    @Transactional
    public void addGroup(Integer facultyId, Group group) {
        Faculty faculty = facultyRepository.findById(facultyId).orElseThrow(() -> new NotFoundException("Can't find faculty"));
        faculty.getGroups().add(group);
        //return group;
    }

    @Override
    public Faculty findMostPopularFaculty(Integer universityId) {
        List<Faculty> faculties = new ArrayList<>();
        faculties = facultyRepository.findAll()
                .stream()
                .filter(faculty -> faculty.getUniversityId() == universityId)
                .collect(Collectors.toList());
        Integer max = faculties.stream().mapToInt(faculty -> faculty.getGroups().size())
                .max().orElseThrow(NoSuchElementException::new);
         faculties = faculties.stream().filter(faculty -> faculty.getGroups().size() == max)
                 .sorted(Comparator.comparing(Faculty::getTitle))
                 .collect(Collectors.toList());
        return facultyRepository.findById(faculties.get(0).getId())
                .orElseThrow(() -> new NotFoundException("Can't find most popular faculty"));
    }

    @Override
    public Faculty deleteById(Integer id) {
        Faculty faculty = facultyRepository.findById(id).orElseThrow(() -> new NotFoundException("Can't find faculty"));
        facultyRepository.deleteById(id);
        return  faculty;
    }
}
