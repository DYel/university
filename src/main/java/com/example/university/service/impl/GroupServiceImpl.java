package com.example.university.service.impl;

import com.example.university.exceptions.NotFoundException;
import com.example.university.model.Faculty;
import com.example.university.model.Group;
import com.example.university.repository.FacultyRepository;
import com.example.university.repository.GroupRepository;
import com.example.university.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {
    private final GroupRepository groupRepository;
    private final FacultyRepository facultyRepository;
    @Override
    public Group create(Group group) {
        group.setId(UUID.randomUUID());
        return groupRepository.save(group);
    }

    @Override
    public Group update(Group group) {
        return groupRepository.save(group);
    }

    @Override
    public Group findById(Integer universityId, Integer facultyId,  UUID id) {
       Group group = groupRepository.findById(id).orElseThrow(() -> new NotFoundException("Can't find group with id" + id));
        if (!group.getUniversityId().equals(universityId)) {
            throw new NotFoundException("Incorrect universityId:" + universityId);
        } else if (!group.getFacultyId().equals(facultyId)) {
            throw new NotFoundException("Incorrect facultyId:" + facultyId);
        }
        return group;
    }

    @Override
    public Group deleteById(UUID id) {
        Group group = groupRepository.findById(id).orElseThrow(() -> new NotFoundException("Can't find group with id" + id));
        groupRepository.deleteById(id);
        return group;
    }
}
