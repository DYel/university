package com.example.university.service.impl;

import com.example.university.exceptions.NotFoundException;
import com.example.university.model.University;
import com.example.university.repository.UniversityRepository;
import com.example.university.service.UniversityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UniversityServiceImpl implements UniversityService {
    private final UniversityRepository universityRepository;
    @Override
    public University create(University university) {
        return universityRepository.save(university);
    }

    @Override
    public University findById(Integer id) {
        return universityRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("No results for id " + id));
    }

    @Override
    public University update(University university) {
        return universityRepository.save(university);
    }

    @Override
    public University deleteById(Integer id) {
        University university = findById(id);
        universityRepository.deleteById(id);
        return university;
    }
}
