package com.example.university.service;

import com.example.university.dto.CreateUniversityDto;
import com.example.university.model.University;

public interface UniversityService {
    University create(University university);

    University findById(Integer id);

    University update(University university);

    University deleteById(Integer id);


}
