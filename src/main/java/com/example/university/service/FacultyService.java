package com.example.university.service;

import com.example.university.dto.FacultyDto;
import com.example.university.model.Faculty;
import com.example.university.model.Group;
import com.example.university.model.University;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface FacultyService {
    Faculty create (Faculty faculty);

    Faculty update(Faculty faculty);
    Faculty findById(Integer universityId, Integer id);
    void addGroup(Integer facultyId, Group group);

    Faculty deleteById(Integer id);

    Faculty findMostPopularFaculty(Integer universityId);
}
