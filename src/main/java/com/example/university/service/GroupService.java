package com.example.university.service;

import com.example.university.model.Group;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public interface GroupService {

    Group create (Group group);

    Group findById(Integer universityId, Integer facultyId,  UUID id);

    Group deleteById(UUID id);

    Group update(Group group);
}
