package com.example.university.facade;

import com.example.university.dto.CreateGroupDto;
import com.example.university.dto.GroupDto;

import java.util.UUID;

public interface GroupFacade {
    GroupDto create ( Integer universityId, Integer facultyId, CreateGroupDto createGroupDto);

    GroupDto findById(Integer universityId, Integer facultyId, UUID id);

    GroupDto deleteById(UUID id);

    GroupDto update(GroupDto groupDto);
}
