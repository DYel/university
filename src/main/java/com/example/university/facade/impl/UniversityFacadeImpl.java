package com.example.university.facade.impl;

import com.example.university.dto.CreateUniversityDto;
import com.example.university.dto.UniversityDto;
import com.example.university.facade.UniversityFacade;
import com.example.university.model.University;
import com.example.university.service.UniversityService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UniversityFacadeImpl implements UniversityFacade {
    private final ConversionService conversionService;
    private final UniversityService universityService;
    @Override
    public UniversityDto create(CreateUniversityDto createDto) {
        University university = conversionService.convert(createDto, University.class);
        University createdUniversity = universityService.create(university);
        UniversityDto universityDto = conversionService.convert(createdUniversity, UniversityDto.class);
        return universityDto;
    }

    @Override
    public UniversityDto findById(Integer id) {
        University university = universityService.findById(id);
        return conversionService.convert(university, UniversityDto.class);
    }

    @Override
    public UniversityDto update(UniversityDto universityDto) {
        University university = conversionService.convert(universityDto, University.class);
        University updatedUniversity = universityService.update(university);
        return conversionService.convert(updatedUniversity, UniversityDto.class);
    }

    @Override
    public UniversityDto deleteById(Integer id) {
        University deletedUniversity = universityService.deleteById(id);
        return conversionService.convert(deletedUniversity, UniversityDto.class);
    }
}
