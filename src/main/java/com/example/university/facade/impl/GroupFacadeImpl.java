package com.example.university.facade.impl;

import com.example.university.dto.CreateGroupDto;
import com.example.university.dto.GroupDto;
import com.example.university.facade.GroupFacade;
import com.example.university.model.Group;
import com.example.university.service.FacultyService;
import com.example.university.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GroupFacadeImpl implements GroupFacade {
    private final ConversionService conversionService;
    private final GroupService groupService;
    private  final FacultyService facultyService;
    @Override
    public GroupDto create( Integer universityId, Integer facultyId, CreateGroupDto createGroupDto) {
//        if ( facultyService.findById(facultyId).getUniversityId() == universityId ) {
//        }
        Group group = conversionService.convert(createGroupDto, Group.class);
        group.setUniversityId(universityId);
        group.setFacultyId(facultyId);
        group.setId(UUID.randomUUID());
        Group createdGroup = groupService.create(group);
        facultyService.addGroup(facultyId, createdGroup);
        GroupDto groupDto = conversionService.convert(createdGroup, GroupDto.class);
        return groupDto;
    }

    @Override
    public GroupDto findById(Integer universityId, Integer facultyId, UUID id)  {
        Group group = groupService.findById(universityId, facultyId, id);
        return conversionService.convert(group, GroupDto.class);
    }

    @Override
    public GroupDto deleteById(UUID id) {
        Group deletedGroup = groupService.deleteById(id);
        return conversionService.convert(deletedGroup, GroupDto.class);
    }

    @Override
    public GroupDto update(GroupDto groupDto) {
        Group group = conversionService.convert(groupDto, Group.class);
        Group updatedGroup = groupService.update(group);
        return conversionService.convert(updatedGroup, GroupDto.class);
    }
}
