package com.example.university.facade.impl;

import com.example.university.dto.CreateFacultyDto;
import com.example.university.dto.FacultyDto;
import com.example.university.facade.FacultyFacade;
import com.example.university.model.Faculty;
import com.example.university.service.FacultyService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FacultyFacadeImpl implements FacultyFacade {
    private final FacultyService facultyService;

    private  final ConversionService conversionService;
    @Override
    public FacultyDto create(Integer universityId, CreateFacultyDto createFacultyDto) {
        Faculty faculty = conversionService.convert(createFacultyDto, Faculty.class);
        faculty.setUniversityId(universityId);
        Faculty createdFaculty = facultyService.create(faculty);
        FacultyDto facultyDto = conversionService.convert(createdFaculty, FacultyDto.class);
        return facultyDto;
    }

    @Override
    public FacultyDto update(FacultyDto facultyDto) {
        Faculty faculty = conversionService.convert(facultyDto, Faculty.class);
        Faculty updatedFaculty = facultyService.update(faculty);
        return conversionService.convert(updatedFaculty, FacultyDto.class);
    }

    @Override
        public FacultyDto findById( Integer universityId, Integer id){
//        if (universityService.findById(universityId) != null
//                || universityService.findById(universityId).getFaculties().stream().anyMatch(f -> (f.getId() == id))) {
//            Faculty faculty = facultyService.findById(id);
//return conversionService.convert(faculty, FacultyDto.class);
//        }
Faculty faculty = facultyService.findById(universityId, id);
return conversionService.convert(faculty, FacultyDto.class);
    }

    @Override
    public FacultyDto findMostPopularFaculty(Integer universityId) {
        Faculty faculty = facultyService.findMostPopularFaculty(universityId);
        return conversionService.convert(faculty, FacultyDto.class);
    }

    @Override
    public FacultyDto deleteById(Integer id) {
        Faculty deletedFaculty = facultyService.deleteById(id);
        return conversionService.convert(deletedFaculty, FacultyDto.class);
    }
}
