package com.example.university.facade;

import com.example.university.dto.CreateUniversityDto;
import com.example.university.dto.UniversityDto;
import com.example.university.model.University;

public interface UniversityFacade {
    UniversityDto create(CreateUniversityDto createUniversityDto);

    UniversityDto findById(Integer id);

    UniversityDto update (UniversityDto universityDto);

    UniversityDto deleteById(Integer id);
}
