package com.example.university.facade;

import com.example.university.dto.CreateFacultyDto;
import com.example.university.dto.FacultyDto;

public interface FacultyFacade {
    FacultyDto create(Integer id, CreateFacultyDto createFacultyDto);

    FacultyDto update(FacultyDto facultyDto);

    FacultyDto findById(Integer universityId, Integer id);

    FacultyDto deleteById(Integer id);

    FacultyDto findMostPopularFaculty(Integer universityId);
}
