package com.example.university.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="faculties")
@Builder
public class Faculty {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    @Column(name="university_id")
    private Integer universityId;

//    @OneToMany(orphanRemoval = true)
    @OneToMany
    @JoinColumn(name = "faculty_id", updatable = false, insertable = false,  nullable = false)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    List<Group> groups = new ArrayList<>();
}
