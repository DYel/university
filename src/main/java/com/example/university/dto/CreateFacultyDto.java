package com.example.university.dto;

import com.example.university.model.Faculty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateFacultyDto {
    String title;
    //Integer universityId;
}
