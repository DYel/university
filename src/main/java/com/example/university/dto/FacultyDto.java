package com.example.university.dto;

import com.example.university.model.University;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FacultyDto {
    private Integer id;
    private String title;
    private Integer universityId;
    List<GroupDto> groupsDto;



}
