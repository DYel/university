package com.example.university.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UniversityDto {
    Integer id;
    String title;
    String address;
    String email;

    List<FacultyDto> facultiesDto;
}
